#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <Servo.h> // Servo control
#include <Wire.h> // I2C comms
#include <FS.h> // SPIFFS (flash) fiilesystem for logging
#include "libBmp280.h" // Altitude sensor
#include "requests.h" // Web server request handler
#include "RocketBot.h"

AsyncWebServer server(80);
Servo servo;
Bmp280 bmp;
const char *ssid = "RocketBot";
char led_state; // 0 or 1
char led_count; // How long LED has been in this state
char flight_state; // 'M'anual or P'ad or A'scending' or 'D'escending or 'L'anded or 'E'
char servo_state;  // 'O'pen or 'C'losed
char logging_state; // 'Y' or 'N'
double altc, altm;
double agl; // Average ground level
double ema_factor=0.1; // Exponetial moving average factor for AGL
unsigned long dlen=0; // Stored data points
unsigned long now, next_sample_time; // Current time, and next time to get busy.
unsigned long dur, max_dur=0, loops=0; // Timing vars
char altc_buffer[10];
char altm_buffer[10];
char temp_buffer[10];
char dlen_buffer[10]="None";
File data_file;

// Initialise everything -----------------------------------------------
void setup() {
  // Set initial states
  flight_state='M';
  servo_state='C';
  logging_state='N';

   // Set up serial debugging
  Serial.begin(115200);
  while(!Serial);
  Serial.println("\nI'm alive!");

 // Connect LED
  pinMode(PIN_STATUS_LED, OUTPUT); // Set Vcc pin to high impedance so it doesn't mind having 3.7V on it.
  led_state=led_count=1; // Initialise LED state and count (strictly led_count should be 0, but meh)
  digitalWrite(PIN_STATUS_LED, led_state); // Turn LED on

 // Connect servo
  servo.attach(PIN_SERVO_CTL); // attaches the servo to pin
  servo.write(SERVO_CLOSE);

  // Connect bmp
  pinMode(PIN_BMP_GND, OUTPUT);
  digitalWrite(PIN_BMP_GND, 0); // Provide 0V for BMP280 ground
  pinMode(PIN_BMP_VCC, OUTPUT);
  digitalWrite(PIN_BMP_VCC, 1); // Provide 3.3V for BMP280 Vcc
  delay(5); // Allow VCC and GND pins to settle
  Serial.println("BMP pins set.");
  if(bmp.init(OSRS_P, PIN_BMP_SDA, PIN_BMP_SCL)) {
    flight_state='E';
    agl=0.0;
  }
  else {
    bmp.update();
    altc=altm=bmp.altitude();
    sprintf(altc_buffer, "%0.1f", altc);
    sprintf(altm_buffer, "%0.1f", altm);
    sprintf(temp_buffer, "%0.1f", bmp.temperature());
}
  //  agl=bmp.altitude(bmp.pressure()); // Initial reading for average ground level

  pinMode(D4, OUTPUT);
  digitalWrite(D4, 1); // Turn LED on

  // Set up access point
  Serial.print("Configuring access point... ");
  WiFi.softAP(ssid); // No password
  IPAddress ap_addr = WiFi.softAPIP();
  Serial.println(ap_addr);

  // Set up server routes
  initServer();
  server.begin();

  // Set up filesystem in flash
  // Make sure Arduino IDE Tools/Flash Size is 4M (2M SPIFFS). This gives
  // enough space for 2MB/16B=131,000 samples, or over 60 mins of logging.
  if(!SPIFFS.begin()) {
    delay(1000); // Give serial monitor a chance to start
    Serial.println("Formatting SPIFFS...");
    SPIFFS.format();
    if(!SPIFFS.begin())
      Serial.println("...failed!!!");
    else
      Serial.println("...completed.");
  }

  // Get busy on first loop
  next_sample_time=micros()+SAMPLE_DELAY;
}

// Main loop -----------------------------------------------------------
void loop() { // Mesured to loop over 140,000 times a second
  now=micros();
  if(now>=next_sample_time) {
	if(++led_count>=(flight_state=='M'?MANUAL_LED_RATE:FLIGHT_LED_RATE)) {
	  led_state^=1;
	  digitalWrite(PIN_STATUS_LED, led_state);
	  led_count=0;
    }
    if(flight_state=='F') {
      bmp.update();
      altc=bmp.altitude();
      sprintf(altc_buffer, "%0.1f", altc);
      if(altc>altm) {
        altm=altc;
        sprintf(altm_buffer, "%0.1f", altm);
      }
      sprintf(temp_buffer, "%0.1f", bmp.temperature());
      //data_file.printf("%ld\t%s\t%s\n", now, altc_buffer, temp_buffer);
      data_file.printf("%ld\t%ld\t%ld\n", now, bmp.ucp, bmp.uct);
      ltoa(++dlen, dlen_buffer, 10);
      // Output for serial plotter
      //Serial.println(altc_buffer);
      //Serial.print(",");
      //Serial.println(temp_buffer);
    }
    next_sample_time+=SAMPLE_DELAY;
  }
}

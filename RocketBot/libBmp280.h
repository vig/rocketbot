#ifndef libBmp280_h
#define libBmp280_h

// libBmp280.h - Library for BMP280 Sensor.
// By Greg Fawcett <greg@vig.co.nz>
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

// I²C Configuration
// I²C interface is selected when pin 5 (CSB) is high. This is already done by an
// on-board pull-up resistor, so leave pin 5 disconnected for I²C.
// In I²C mode:
//  - SCK pin becomes SCL
//  - SDI pin becomes SDA
//  - SDO pin becomes address select, leave unconnected for 0x76, or connect to 3.3V for 0x77.

#include <Wire.h>
#include <Arduino.h>

#define LIBBMP280_DEBUG 0

#define BMP280_CHIP_ID1			0x56
#define BMP280_CHIP_ID2			0x57
#define BMP280_CHIP_ID3			0x58
#define BMP280_ADDRESS			0x76 // 0x76 if SDO low else 0x77
#define BMP280_FORCED			0x21 // osrs_t 1 and forced mode

#define BMP280_REG_CALIB		0x88
#define BMP280_REG_ID			0xD0
#define BMP280_REG_RESET		0xE0
#define BMP280_REG_STATUS		0xF3
#define BMP280_REG_CONTROL		0xF4
#define BMP280_REG_CONFIG		0xF5
#define BMP280_REG_DATA			0xF7

#define bmp_oversampling(osrs_p) ((osrs_p)>4 ? 16 : ((osrs_p) ? 1<<(osrs_p-1) : 0))

// BMP280 Pressure Sensor class
class Bmp280
{

    public:
	// Constructor
	Bmp280(void);
	~Bmp280(void);
	// Set up comms to sensor and set accuracy.
	// Returns 0 if OK or error code.
	uint8_t init(uint8_t accuracy, int sda_pin, int scl_pin);
	// Converts accuracy to oversampling count.
	uint8_t oversampling(uint8_t accuracy);
	// Read uncalibrated sensor data
	int8_t update(void);
	// Get altitude in metres
	double altitude(void);
	// Get pressure in pascals
	double pressure(void);
	// Get temperature in celcius
	double temperature(void);
  uint32_t ucp, uct; // Temporarily public so we can log them

    private:
	const uint8_t read_delay[6] = {0, 7, 9, 14, 23, 44};
	uint8_t control_request;
	uint8_t data[24]; // Shared array for I2C comms
	// uint32_t ucp, uct;
	// Calibration values
	uint16_t dig_T1, dig_P1;
	int16_t dig_T2 , dig_T3 , dig_T4 , dig_P2 , dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
	int32_t t_fine;

	uint8_t osrs_p;
//	uint8_t _pra_cnt, _pra_idx;
//	uint32_t _pra_sum;

//	int32_t _getB5(void);
//	uint32_t _read(uint8_t addr, uint8_t bytes);
//	void _write(uint8_t addr, uint8_t data);
//	void _pra_clear(void);
//	void _pra_add(uint32_t value);
//	uint32_t _pra_average();

	int8_t write_data(uint8_t count);
	int8_t read_data(uint8_t count);
	int16_t get_chip_id(void);
	int8_t get_calibration(void);
};

#endif

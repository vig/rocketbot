#ifndef RocketBot_h
#define RocketBot_h

#define VERSION "0.2"

// How long between pressure readings, in microseconds
#define SAMPLE_DELAY 31250 // 32/sec
//#define SAMPLE_DELAY 62500 // 16/sec
//#define SAMPLE_DELAY 1000000 // 1/sec
#define SAMPLE_RATE 1000000/SAMPLE_DELAY
#define MANUAL_LED_RATE SAMPLE_RATE>>1
#define FLIGHT_LED_RATE SAMPLE_RATE>>3

// OSRS_P - pressure accuracy (0 - 7)
// POSC - pressure over-sampling count
//
#define OSRS_P 1
#if OSRS_P==0
  #define POSC 0
#elif OSRS_P==1
  #define P_OSS 1
#elif OSRS_P==2
  #define P_OSS 2
#elif OSRS_P==3
  #define P_OSS 4
#elif OSRS_P==4
  #define P_OSS 8
#else
  #define P_OSS 16
#endif

// Pin definitions
#define PIN_STATUS_LED D4
#define PIN_SERVO_CTL D3
// Boot control pins D3, D4 and D8 and comms pins Rx and TX limit the choice
// of four consecutive pins for the BMP280.
// - 3V3-D8-D7-D6 fails to powerup because of D8 (although works on reset).
// - D2-D1-RX-TX prevents USB connection for uploading.
// D0-D5-D6-D7 is the only workable option.
#define PIN_BMP_VCC D0
#define PIN_BMP_GND D5
#define PIN_BMP_SCL D6
#define PIN_BMP_SDA D7

// Servo open and close settings
#define SERVO_OPEN 0
#define SERVO_CLOSE 90
#endif

// libBmp280.cpp - Library for BMP280 Sensor.
// By Greg Fawcett <greg@vig.co.nz>
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

#include "libBmp280.h"

//----------------------------------------------------------------------
// BMP280 Pressure Sensor class

//----------------------------------------------------------------------
// Constructor
Bmp280::Bmp280(void) {
}

//----------------------------------------------------------------------
Bmp280::~Bmp280() {
}

//----------------------------------------------------------------------
// Initialise BMP280
// Accuracy is equivalent to the osrs_p setting in the BMP280 datasheet:
//   0: No samples taken (measurement skipped)
//   1: 1 sample
//   2: 2 samples
//   3: 4 samples
//   4: 8 samples
//   5: 16 samples
uint8_t Bmp280::init(uint8_t accuracy, int sda_pin, int scl_pin) {
	osrs_p=accuracy;
	Wire.begin(sda_pin, scl_pin);
	// Check everything is working by getting chip ID.
	int16_t chip_id=get_chip_id();
#if (LIBBMP280_DEBUG==1)
	Serial.print("Chip ID:"); Serial.println(chip_id);
#endif
	if(chip_id!=BMP280_CHIP_ID1&&chip_id!=BMP280_CHIP_ID2&&chip_id!=BMP280_CHIP_ID3)
		return 1; // Bad chip ID
	// Get calibration parameters
	get_calibration();
	// Set control and config registers.
	data[0]=BMP280_REG_CONTROL;
	data[1]=0; // Skip p and t measurement, sleep mode.
	data[2]=0; // No tstandby, no filter, no spi.
	write_data(3);
	return 0; // All good
}

//----------------------------------------------------------------------
// Get uncompensated pressure and temp (ucp and uct) values
int8_t Bmp280::update(void) {
	data[0]=BMP280_REG_CONTROL;
	data[1]=osrs_p<<2 | BMP280_FORCED;
	write_data(2);
	delay(read_delay[osrs_p]);
	data[0]=BMP280_REG_DATA;
	if(read_data(6)==-1)
		return -1;
	ucp = (int32_t)((((uint32_t)(data[0]))<<12) | (((uint32_t)(data[1]))<<4) | ((uint32_t)data[2]>>4));
	uct = (int32_t)((((int32_t)(data[3]))<<12) | (((int32_t)(data[4]))<<4) | (((int32_t)(data[5]))>>4));

#if (LIBBMP280_DEBUG)
	Serial.print("Read ucp "); Serial.print(ucp); Serial.println(" reset to 415148");
	Serial.print("Read uct "); Serial.print(uct); Serial.println(" reset to 519888");
	// Bosch calc test values
	// ucp=415148;
	// uct=519888;
#endif
	// Calculate t_fine
	int32_t x=(uct>>4)-(int32_t)dig_T1;
	t_fine = (((((uct>>3)-((int32_t)dig_T1<<1)))*(int32_t)dig_T2)>>11)+((((x*x)>>12)*(int32_t)dig_T3)>>14);
#if (LIBBMP280_DEBUG)
	Serial.print("t_fine "); Serial.println(t_fine);
#endif
	return 0;
}

//----------------------------------------------------------------------
// Get temperature in celcius
double Bmp280::temperature(void) {
	return ((t_fine*5+128)>>8)/(double)100;
}

//----------------------------------------------------------------------
// Get pressure in Pascals

double Bmp280::pressure(void) {
	double var1, var2, pressure = 0;

	var1 = (t_fine / 2.0) - 64000.0;
	var2 = var1 * var1 * (dig_P6) / 32768.0;
	var2 = var2 + var1 * (dig_P5) * 2.0;
	var2 = (var2 / 4.0) + ((dig_P4) * 65536.0);
	var1 = ((dig_P3) * var1 * var1 / 524288.0
	+ (dig_P2) * var1) / 524288.0;
	var1 = (1.0 + var1 / 32768.0) * (dig_P1);
	pressure = 1048576.0 - (double) ucp;
	if (var1 != 0) {
		pressure = (pressure - (var2 / 4096.0)) * 6250.0 / var1;
		var1 = (dig_P9) * pressure * pressure / 2147483648.0;
		var2 = pressure * (dig_P8) / 32768.0;
		pressure = pressure + (var1 + var2 + (dig_P7)) / 16.0;
	} else {
		pressure = 0;
	}
	return pressure;
}


//----------------------------------------------------------------------
// Get altitude in metres
// Sealevel pressure: 101325 Pascals
double Bmp280::altitude(void) {
	return 44330*(1.0-pow(pressure()/(double)101325, 0.1903));
}

//----------------------------------------------------------------------
// Write count bytes to from data array to address in data[0].
// Returns 0 if good else -1 on timeout.
int8_t Bmp280::write_data(uint8_t count) {

//	Serial.print("write_data() "); Serial.print(count); Serial.print(" to 0x"); Serial.println(data[0], HEX);
//	uint8_t i;
//	for(i=1; i<count; ++i) {
//		Serial.print('\t'); Serial.print(i); Serial.print(":"); Serial.println(data[i], HEX);
//	}
	Wire.beginTransmission(BMP280_ADDRESS);
	Wire.write(data, count);
	return Wire.endTransmission() ? -1 : 0;
}

//----------------------------------------------------------------------
// Read count bytes from address in data[0] into data array.
// Returns 0 if good else -1 on timeout.
int8_t Bmp280::read_data(uint8_t count) {
	unsigned long started = millis();
//	Serial.print("read_data() "); Serial.print(count); Serial.print(" from 0x"); Serial.println(data[0], HEX);
	uint8_t i;
	Wire.beginTransmission(BMP280_ADDRESS);
	Wire.write(data[0]);
	Wire.endTransmission();

	Wire.requestFrom((uint8_t)BMP280_ADDRESS, count);
	while(millis()-started<1000) {
		if(Wire.available()==count) {
			for(i=0; i<count; ++i) {
				data[i]=Wire.read();
//				Serial.print('\t'); Serial.print(i); Serial.print(":"); Serial.println(data[i], HEX);
			}
			return 0;
		}
	}
	return -1; // Timed out
}

//----------------------------------------------------------------------
// Return chip ID, or -1 if timeout.
int16_t Bmp280::get_chip_id(void) {
	data[0]=BMP280_REG_ID;
	return read_data(1)==-1 ? -1 : (int16_t) data[0];
}

//----------------------------------------------------------------------
// Return chip ID, or -1 if timeout.
int8_t Bmp280::get_calibration(void) {
	data[0]=BMP280_REG_CALIB;
	if(read_data(24)==-1)
		return -1;

	dig_T1=(uint16_t) ((data[1]<<8)|data[0]);
	dig_T2=(int16_t) ((data[3]<<8)|data[2]);
	dig_T3=(int16_t) ((data[5]<<8)|data[4]);
	dig_P1=(uint16_t) ((data[7]<<8)|data[6]);
	dig_P2=(int16_t) ((data[9]<<8)|data[8]);
	dig_P3=(int16_t) ((data[11]<<8)|data[10]);
	dig_P4=(int16_t) ((data[13]<<8)|data[12]);
	dig_P5=(int16_t) ((data[15]<<8)|data[14]);
	dig_P6=(int16_t) ((data[17]<<8)|data[16]);
	dig_P7=(int16_t) ((data[19]<<8)|data[18]);
	dig_P8=(int16_t) ((data[21]<<8)|data[20]);
	dig_P9=(int16_t) ((data[23]<<8)|data[22]);
#if (LIBBMP280_DEBUG)
	Serial.print("Set control: 0x"); Serial.print(0x21|osrs_p<<2, HEX);
	Serial.print(" ("); Serial.print(0x21|osrs_p<<2, BIN); Serial.println(")");
	// Sample values from datasheet to test calculations
	Serial.print("dig_T1 "); Serial.print(dig_T1); Serial.println(" reset to 27504");
	Serial.print("dig_T2 "); Serial.print(dig_T2); Serial.println(" reset to 26435");
	Serial.print("dig_T3 "); Serial.print(dig_T3); Serial.println(" reset to -1000");
	Serial.print("dig_P1 "); Serial.print(dig_P1); Serial.println(" reset to 36477");
	Serial.print("dig_P2 "); Serial.print(dig_P2); Serial.println(" reset to -10685");
	Serial.print("dig_P3 "); Serial.print(dig_P3); Serial.println(" reset to 3024");
	Serial.print("dig_P4 "); Serial.print(dig_P4); Serial.println(" reset to 2855");
	Serial.print("dig_P5 "); Serial.print(dig_P5); Serial.println(" reset to 140");
	Serial.print("dig_P6 "); Serial.print(dig_P6); Serial.println(" reset to -7");
	Serial.print("dig_P7 "); Serial.print(dig_P7); Serial.println(" reset to 15500");
	Serial.print("dig_P8 "); Serial.print(dig_P8); Serial.println(" reset to -14600");
	Serial.print("dig_P9 "); Serial.print(dig_P9); Serial.println(" reset to 6000");
	// Bosch calc test values
	// dig_T1 = 27504;
	// dig_T2 = 26435;
	// dig_T3 = -1000;
	// dig_P1 = 36477;
	// dig_P2 = -10685;
	// dig_P3 = 3024;
	// dig_P4 = 2855;
	// dig_P5 = 140;
	// dig_P6 = -7;
	// dig_P7 = 15500;
	// dig_P8 = -14600;
	// dig_P9 = 6000;
#endif

	return 0;
}
